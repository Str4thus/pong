#include <iostream>
#include "util.h"

namespace util {
	float random(float min, float max) {
		return min + static_cast<float>(std::rand()) / (static_cast<float>(RAND_MAX / (max - min)));
	}

	float clamp(float val, float min, float max) {
		if (val < min)
			val = min;
		else if (val > max)
			val = max;

		return val;
	}
}