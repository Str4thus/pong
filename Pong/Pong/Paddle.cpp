#include "Paddle.h"



Paddle::Paddle(float width, float height, sf::Vector2f startPosition) : RectangleShape(sf::Vector2f(width, height)) {
	RectangleShape::setOrigin(width / 2, height / 2);
	RectangleShape::setFillColor(sf::Color::Black);
	this->init(startPosition);
	RectangleShape::update();
}


Paddle::~Paddle() {}

void Paddle::init(sf::Vector2f startPosition) {
	RectangleShape::setPosition(startPosition);
}

void Paddle::moveUp(float dt) {
	if (RectangleShape::getPosition().y - RectangleShape::getSize().y / 2 > 0) {
		float vMove = this->velocity * dt * 100;
		RectangleShape::move(sf::Vector2f(0, -vMove));
	}
}

void Paddle::moveDown(float dt, float bottom) {
	if (RectangleShape::getPosition().y + RectangleShape::getSize().y / 2 < bottom) {
		float vMove = this->velocity * dt * 100;
		RectangleShape::move(sf::Vector2f(0, vMove));
	}

}