#include "Ball.h"

Ball::Ball(float radius) : CircleShape(radius) {
	CircleShape::setOrigin(radius, radius);
	CircleShape::setFillColor(sf::Color::Red);
	CircleShape::update();

	if (!this->paddleBounceSoundBuffer.loadFromFile("res/sounds/PaddleBounce.wav"))
		std::cerr << "Could not load paddle bounce sound!" << std::endl;

	if (!this->wallBounceSoundBuffer.loadFromFile("res/sounds/WallBounce.wav"))
		std::cerr << "Could not load wall bounce sound!" << std::endl;

	this->paddleBounceSound.setBuffer(paddleBounceSoundBuffer);
	this->wallBounceSound.setBuffer(wallBounceSoundBuffer);
}


Ball::~Ball() {}

void Ball::init(sf::Vector2f startPosition) {
	CircleShape::setPosition(startPosition);
	this->velocity = this->START_SPEED;
	this->angle = util::random(this->MIN_ANGLE, this->MAX_ANGLE);
	this->hDirection = util::random(0, 1) > 0.5 ? 1 : -1;
	this->vDirection = util::random(0, 1) > 0.5 ? 1 : -1;
}

void Ball::move(float dt) {
	float hMove = cos(this->angle) * velocity * dt * this->SPEED_FACTOR * this->hDirection;
	float vMove = -sin(this->angle) * velocity * dt * this->SPEED_FACTOR * this->vDirection;

	CircleShape::move(hMove, vMove);
}

void Ball::checkBorderCollision(sf::Vector2u windowSize, float winZoneWidth, State& gamestate) {
	float lowerBorder = CircleShape::getPosition().y + CircleShape::getRadius();
	float upperBorder = CircleShape::getPosition().y - CircleShape::getRadius();
	float rightBorder = CircleShape::getPosition().x + CircleShape::getRadius();
	float leftBorder = CircleShape::getPosition().x - CircleShape::getRadius();


	if (upperBorder <= 0 || lowerBorder >= windowSize.y) {
		this->bounceFromBorder();
	}

	// Right Scored
	if (leftBorder <= winZoneWidth) {
		gamestate.rightScore++;
		gamestate.active = false;
	}

	// Left Scored
	if (rightBorder >= (windowSize.x - winZoneWidth)) {
		gamestate.leftScore++;
		gamestate.active = false;
	}
}

void Ball::checkPaddleCollision(sf::RectangleShape paddle, bool isLeft) {
	sf::Vector2f ballPosition = CircleShape::getPosition();
	sf::Vector2f paddlePosition = paddle.getPosition();

	float ballUpperBorder = ballPosition.y + CircleShape::getRadius();
	float ballLowerBorder = ballPosition.y - CircleShape::getRadius();
	float ballRightBorder = ballPosition.x + CircleShape::getRadius();
	float ballLeftBorder = ballPosition.x - CircleShape::getRadius();

	float paddleUpperBorder = paddlePosition.y + paddle.getSize().y / 2;
	float paddleLowerBorder = paddlePosition.y - paddle.getSize().y / 2;
	float paddleRightBorder = paddlePosition.x + paddle.getSize().x / 2;
	float paddleLeftBorder = paddlePosition.x - paddle.getSize().x / 2;

	if (ballLowerBorder <= paddleUpperBorder
		&& ballUpperBorder >= paddleLowerBorder
		&& ((ballLeftBorder <= paddleRightBorder && ballLeftBorder >= paddleLeftBorder && isLeft) 
			|| (ballRightBorder >= paddleLeftBorder && ballRightBorder <= paddleRightBorder && !isLeft))) {

		if (ballPosition.y >= paddlePosition.y)
			this->bounceFromPaddle(false);
		else
			this->bounceFromPaddle(true);
	}
}

void Ball::bounceFromBorder() {
	this->wallBounceSound.play();
	this->vDirection = this->vDirection == 1 ? -1 : 1;
	
	this->angle += util::random(-0.08726646, 0.08726646); // -5 and 5
	this->angle = util::clamp(this->angle, this->MIN_ANGLE, this->MAX_ANGLE);

	this->increaseVelocity();
	this->changeColor();
}

void Ball::bounceFromPaddle(bool hitUpperHalf) {
	this->paddleBounceSound.play();
	this->hDirection = this->hDirection == 1 ? -1 : 1;
	
	if (hitUpperHalf) {
		this->vDirection = util::random(0, 1) > 0.7f ? 1 : this->vDirection;
	}
	else {
		this->vDirection = util::random(0, 1) > 0.7f ? -1 : this->vDirection;
	}

	this->angle = util::clamp(this->angle, this->MIN_ANGLE, this->MAX_ANGLE);

	this->angle += util::random(-0.08726646, 0.08726646); // -5 and 5
	this->increaseVelocity();
	this->changeColor();
}

void Ball::increaseVelocity() {
	std::cout << this->velocity << std::endl;

	if (this->velocity < this->MAX_SPEED) {
		this->velocity += this->SPEED_STEP;
	
		if (this->velocity > this->MAX_SPEED)
			this->velocity = this->MAX_SPEED;
	}
}

void Ball::changeColor() {
	int numberOfColors = sizeof(colors) / sizeof(colors[0]);
	CircleShape::setFillColor(this->colors[(int) util::random(0, 7)]);
}