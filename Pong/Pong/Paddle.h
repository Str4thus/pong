#pragma once

#include <SFML/Graphics.hpp>

class Paddle : public sf::RectangleShape
{
public:
	Paddle(float width, float height, sf::Vector2f startPosition);
	~Paddle();

	void init(sf::Vector2f startPosition);

	void moveUp(float dt);
	void moveDown(float dt, float bottom);

private:
	float velocity = 10.f;
};

