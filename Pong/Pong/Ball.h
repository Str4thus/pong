#pragma once

#include <iostream>
#include <math.h>

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "../util.h"

class Ball : public sf::CircleShape
{
public:
	Ball(float radius);
	~Ball();

	void init(sf::Vector2f startPosition);
	void move(float dt);
	void checkBorderCollision(sf::Vector2u windowSize, float winZoneWidth, State& gamestate);
	void checkPaddleCollision(sf::RectangleShape paddle, bool isLeft);

private:
	const float MAX_ANGLE = 1.047198f; // 60 deg
	const float MIN_ANGLE = -1.047198f; // -60 deg

	const float MAX_SPEED = 90.f;
	const float START_SPEED = 40.f;
	const float SPEED_STEP = 2.f;

	const float SPEED_FACTOR = 10.f;

	const sf::Color colors[7] = {sf::Color::Red, sf::Color::Green, sf::Color::Black, sf::Color::Black, 
		sf::Color::Cyan, sf::Color::Magenta, sf::Color::Yellow};

	sf::SoundBuffer paddleBounceSoundBuffer;
	sf::SoundBuffer wallBounceSoundBuffer;

	sf::Sound paddleBounceSound;
	sf::Sound wallBounceSound;

	float velocity;
	float angle;
	int hDirection; // 1 = right | -1 = left
	int vDirection; // 1 = down | -1 = up


	void bounceFromBorder();
	void bounceFromPaddle(bool hitUpperHalf);
	void increaseVelocity();
	void changeColor();
};

