#include <iostream>
#include <sstream>
#include <ctime>
#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "Pong/Pong.h"

const float WINDOW_WIDTH = 1000;
const float WINDOW_HEIGHT = 600;

const float PADDLE_WIDTH = 10.f;
const float PADDLE_HEIGHT = 200.f;

const float BALL_RADIUS = 7;

const float WIN_ZONE_WIDTH = 20.f;
const float PADDLE_BUFFER_SPACE = 30.f;

const float WINNING_SCORE = 12;

const sf::Vector2f CENTER_POINT(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2);
const sf::Vector2f LEFT_PLAYER_SCORE_POSITION(100.f, 30.f);
const sf::Vector2f RIGHT_PLAYER_SCORE_POSITION(WINDOW_WIDTH - 100.f, 30.f);
const sf::Vector2f WINNER_TEXT_POSITION(CENTER_POINT + sf::Vector2f(0, -80.f));


int main()
{
	srand((unsigned)time(0));

	// Music
	sf::Music backgroundMusic;
	;
	if (!backgroundMusic.openFromFile("res/sounds/Background.wav")) {
		std::cerr << "Could not load background music!" << std::endl;
	}
	backgroundMusic.play();

	// DeltaTime
	sf::Clock deltaClock;

	// Window
	sf::RenderWindow window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Pong!");
	window.setFramerateLimit(120);

	// Texts
	sf::FloatRect textBounds; // For centering the text origin

	sf::Font font;
	if (!font.loadFromFile("res/fonts/default.ttf")) {
		std::cerr << "Could not load default font!" << std::endl;
	}

	sf::Text startText("Press Space to Start!", font);
	textBounds = startText.getLocalBounds();
	startText.setOrigin(textBounds.left + textBounds.width / 2, textBounds.top + textBounds.height / 2);
	startText.setPosition(CENTER_POINT);
	startText.setFillColor(sf::Color::Black);

	sf::Text winnerText("Player X has won!", font);
	winnerText.setCharacterSize(50);
	textBounds = winnerText.getLocalBounds();
	winnerText.setOrigin(textBounds.left + textBounds.width / 2, textBounds.top + textBounds.top / 2);
	winnerText.setPosition(WINNER_TEXT_POSITION);
	winnerText.setFillColor(sf::Color::Red);

	sf::Text leftPlayerScoreText("XX", font);
	textBounds = leftPlayerScoreText.getLocalBounds();
	leftPlayerScoreText.setOrigin(textBounds.left + textBounds.width / 2, textBounds.top + textBounds.top / 2);
	leftPlayerScoreText.setPosition(LEFT_PLAYER_SCORE_POSITION);
	leftPlayerScoreText.setFillColor(sf::Color::Red);

	sf::Text rightPlayerScoreText("XX", font);
	textBounds = rightPlayerScoreText.getLocalBounds();
	rightPlayerScoreText.setOrigin(textBounds.left + textBounds.width / 2, textBounds.top + textBounds.top / 2);
	rightPlayerScoreText.setPosition(RIGHT_PLAYER_SCORE_POSITION);
	rightPlayerScoreText.setFillColor(sf::Color::Red);

	// Gamestate
	State* gamestate = new State();

	// Objects
	Ball ball(BALL_RADIUS);
	Paddle paddleLeft(PADDLE_WIDTH, PADDLE_HEIGHT, sf::Vector2f(WIN_ZONE_WIDTH + PADDLE_WIDTH / 2 + PADDLE_BUFFER_SPACE, WINDOW_HEIGHT / 2));
	Paddle paddleRight(PADDLE_WIDTH, PADDLE_HEIGHT, sf::Vector2f(WINDOW_WIDTH - (WIN_ZONE_WIDTH + PADDLE_WIDTH / 2 + PADDLE_BUFFER_SPACE), WINDOW_HEIGHT / 2));

	bool upMoveLeft = false, upMoveRight = false, downMoveLeft = false, downMoveRight = false;
	bool startGame = false;
	bool hasEnded = false;

	while (window.isOpen())
	{
		float dt = deltaClock.restart().asSeconds();

		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::M))
				backgroundMusic.stop();

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && !gamestate->active)
				startGame = true;

			if (event.type == sf::Event::KeyPressed) {
				if (event.key.code == sf::Keyboard::W) {
					upMoveLeft = true;
				}

				else if (event.key.code == sf::Keyboard::S) {
					downMoveLeft = true;
				}

				if (event.key.code == sf::Keyboard::Up) {
					upMoveRight = true;
				}

				else if (event.key.code == sf::Keyboard::Down) {
					downMoveRight = true;
				}
			}

			if (event.type == sf::Event::KeyReleased) {
				if (event.key.code == sf::Keyboard::W) {
					upMoveLeft = false;
				}

				if (event.key.code == sf::Keyboard::S) {
					downMoveLeft = false;
				}

				if (event.key.code == sf::Keyboard::Up) {
					upMoveRight = false;
				}

				else if (event.key.code == sf::Keyboard::Down) {
					downMoveRight = false;
				}
			}
		}

		window.clear(sf::Color::White);
		/* Begin Tick */

		if (startGame) {
			std::cout << gamestate->leftScore << " | " << gamestate->rightScore << std::endl;
			ball.init(CENTER_POINT);
			gamestate->active = true;
			startGame = false;
			hasEnded = false;
		}

		if (gamestate->active) {
			// Input Handling (Paddle Movement)
			if (upMoveLeft)
				paddleLeft.moveUp(dt);

			if (downMoveLeft)
				paddleLeft.moveDown(dt, WINDOW_HEIGHT);


			if (upMoveRight)
				paddleRight.moveUp(dt);

			if (downMoveRight)
				paddleRight.moveDown(dt, WINDOW_HEIGHT);

			// Collision Checks
			ball.checkPaddleCollision(paddleLeft, true);
			ball.checkPaddleCollision(paddleRight, false);
			ball.checkBorderCollision(window.getSize(), WIN_ZONE_WIDTH, (*gamestate));
			

			// Ball Movement
			ball.move(dt);
		}

		if (!gamestate->active) {
			int start, end;

			std::stringstream leftPlayerScore;
			leftPlayerScore << "0" << gamestate->leftScore;
			start = leftPlayerScore.str().length() - 2;
			end = leftPlayerScore.str().length();
			leftPlayerScoreText.setString(leftPlayerScore.str().substr(start, end));

			std::stringstream rightPlayerScore;
			rightPlayerScore << "0" << gamestate->rightScore;
			start = rightPlayerScore.str().length() - 2;
			end = rightPlayerScore.str().length();
			rightPlayerScoreText.setString(rightPlayerScore.str().substr(start, end));

			if (gamestate->leftScore == WINNING_SCORE) {
				winnerText.setString("Player 1 has won!");
				hasEnded = true;
			}
			else if (gamestate->rightScore == WINNING_SCORE) {
				winnerText.setString("Player 2 has won!");
				hasEnded = true;
			}
		}

		if (hasEnded) {
			delete gamestate;
			gamestate = new State();
		}

		// Draw
		window.draw(paddleLeft);
		window.draw(paddleRight);
		window.draw(leftPlayerScoreText);
		window.draw(rightPlayerScoreText);

		if (!gamestate->active) window.draw(startText);
		if (gamestate->active) window.draw(ball);

		if (hasEnded) {
			window.draw(winnerText);
		}

		/* End Tick */

		window.display();
	}

	return 0;
}