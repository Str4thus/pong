#pragma once

#include <iostream>
struct State {
	bool active = false;
	int leftScore = 0;
	int rightScore = 0;
};

namespace util {
	const float PI = 3.14159f;

	float random(float min, float max);
	float clamp(float val, float min, float max);
}